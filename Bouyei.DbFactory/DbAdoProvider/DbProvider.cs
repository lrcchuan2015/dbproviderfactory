﻿/*-------------------------------------------------------------
 *   auth: bouyei
 *   date: 2016/4/26 9:19:46
 *contact: 453840293@qq.com
 *profile: www.openthinking.cn
 *    Ltd: Microsoft
 *   guid: 93caaa3a-b22b-4b82-8d92-62d598962222
---------------------------------------------------------------*/
using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Collections.Generic;
using System.Threading;

namespace Bouyei.DbFactory.DbAdoProvider
{
    using DbUtils;
    
    public class DbProvider : DbCommonBuilder, IDbProvider
    {
        #region variable
        private LockParam lParam = new LockParam()
        {
            LockTimeout = 5,//s
            SleepInterval = 1000,
        };
        private bool disposed = false;

        public string DbConnectionString { get; set; }
        
        public ProviderType DbType { get; set; }

        public int LockTimeout
        {
            get { return lParam.LockTimeout; }
            set
            {
                lParam.LockTimeout = value < 5 ? 5 : value;
            }
        }

        #endregion

        #region  dispose

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                if (this.dbConn != null) this.dbConn.Dispose();
                if (this.dbDataAdapter != null) this.dbDataAdapter.Dispose();
                if (this.dbCommand != null) this.dbCommand.Dispose();
                if (this.dbBulkCopy != null) this.dbBulkCopy.Dispose();
                if (this.dbCommandBuilder != null) dbCommandBuilder.Dispose();
                if (this.dbTransaction != null) dbTransaction.Dispose();
            }
            disposed = true;
        }

        #endregion

        #region  structure
        public DbProvider(
            string connectionString,
            ProviderType providerType = ProviderType.SqlServer,
            bool isSingleton = false)
            : base(providerType, isSingleton)
        {
            this.DbType = providerType;
            this.DbConnectionString = connectionString;
        }

        public DbProvider(
            ProviderType providerType = ProviderType.SqlServer,
            bool isSingleton = false)
            : base(providerType, isSingleton)
        {
            this.DbType = providerType;
        }

        public static DbProvider CreateProvider(string connectionString,
            ProviderType providerType=ProviderType.SqlServer)
        {
            return new DbProvider(connectionString, providerType);
        }

        public static DbProvider CreateProvider(
            ConnectionConfig connectionConfiguration)
        {
            return new DbProvider(connectionConfiguration.ToString(),
                connectionConfiguration.DbType);
        }

        #endregion

        #region public
        public ResultInfo<bool, string> Connect(string ConnectionString)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                this.DbConnectionString = ConnectionString;
                try
                {
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    {
                        return new ResultInfo<bool, string>(true, string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    return new ResultInfo<bool, string>(false, ex.ToString());
                }
            }
        }

        public ResultInfo<DataTable, string> Query(Parameter dbParameter)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    using (DbCommand cmd = this.CreateCommand(conn, dbParameter))
                    using (DbDataAdapter adapter = this.CreateAdapter())
                    {
                        DataTable dt = new DataTable();
                        adapter.SelectCommand = cmd;
                        adapter.Fill(dt);
                        return new ResultInfo<DataTable, string>(dt, string.Empty);
                    }                   
                }
                catch (Exception ex)
                {
                    return new ResultInfo<DataTable, string>(null, ex.ToString());
                }
            }
        }

        public ResultInfo<DataSet, string> QueryToSet(Parameter dbParameter)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    using (DbCommand cmd = CreateCommand(conn, dbParameter))
                    using (DbDataAdapter adapter = CreateAdapter())
                    {
                        DataSet ds = new DataSet();
                        adapter.SelectCommand = cmd;
                        adapter.Fill(ds);
                        return ResultInfo<DataSet, string>.Create(ds, string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    return new ResultInfo<DataSet, string>(null, ex.ToString());
                }
            }
        }

        public ResultInfo<int, string> QueryToReader(Parameter dbParameter, Func<IDataReader, bool> rowAction)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    int rows = 0;
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    using (DbCommand cmd = CreateCommand(conn, dbParameter))
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows == false)
                            return ResultInfo<int, string>.Create(0, string.Empty);
                        bool isContinue = false;

                        while (reader.Read())
                        {
                            isContinue = rowAction(reader);
                            if (isContinue == false) break;
                            ++rows;
                        }
                    }
                    return ResultInfo<int, string>.Create(rows, string.Empty);
                }
                catch (Exception ex)
                {
                    return new ResultInfo<int, string>(-1, ex.ToString());
                }
            }
        }

        public ResultInfo<int,string> QueryTo<T>(Parameter dbParameter,Func<T,bool> rowAction)
             where T:new()
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    int rows = 0;
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    using (DbCommand cmd = CreateCommand(conn, dbParameter))
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows == false)
                            return ResultInfo<int, string>.Create(0, string.Empty);

                        bool isContinue = false;
                        while (reader.Read())
                        {
                            T row = reader.DataReaderTo<T>(dbParameter.IgnoreCase);
                            isContinue = rowAction(row);
                            if (isContinue == false) break;
                            ++rows;
                        }
                    }

                    return ResultInfo<int, string>.Create(rows, string.Empty);
                }
                catch (Exception ex)
                {
                    return new ResultInfo<int, string>(-1, ex.ToString());
                }
            }
        }

        public ResultInfo<IDataReader, string> QueryToReader(Parameter dbParameter)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    DbConnection conn = CreateConnection(DbConnectionString);
                    DbCommand cmd = CreateCommand(conn, dbParameter);
                    IDataReader reader = cmd.ExecuteReader();
                    return ResultInfo<IDataReader, string>.Create(reader, string.Empty);
                }
                catch (Exception ex)
                {
                    return ResultInfo<IDataReader, string>.Create(null, ex.ToString());
                }
            }
        }

        public ResultInfo<int, string> ExecuteCmd(Parameter dbParameter)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    using (DbCommand cmd = CreateCommand(conn, dbParameter))
                    {
                        int rt = cmd.ExecuteNonQuery();

                        var rValue = GetReturnParameter(cmd);

                        return ResultInfo<int, string>.Create(rt < 0 ? 0 : rt,
                            rValue == null ? string.Empty : rValue.ToString());
                    }
                }
                catch (Exception ex)
                {
                    return ResultInfo<int, string>.Create(-1, ex.ToString());
                }
            }
        }

        public ResultInfo<int, string> QueryToTable(Parameter dbParameter, DataTable dstTable)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    using (DbCommand cmd = CreateCommand(conn, dbParameter))
                    using (DbDataReader dReader = cmd.ExecuteReader())
                    {
                        int oCnt = dstTable.Rows.Count;

                        dstTable.Load(dReader);

                        return ResultInfo<int, string>.Create(dstTable.Rows.Count - oCnt, string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    return ResultInfo<int, string>.Create(-1, ex.ToString());
                }
            }
        }

        public ResultInfo<int, string> ExecuteTransaction(Parameter dbParameter)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    using (DbTransaction tran = BeginTransaction(conn))
                    {
                        try
                        {
                            using (DbCommand cmd = CreateCommand(conn, dbParameter, tran))
                            {
                                int rt = cmd.ExecuteNonQuery();
                                tran.Commit();

                                var rValue = GetReturnParameter(cmd);

                                return ResultInfo<int, string>.Create(rt < 0 ? 0 : rt,
                                    rValue != null ? rValue.ToString() : string.Empty);
                            }
                        }
                        catch (Exception ex)
                        {
                            tran.Rollback();
                            return ResultInfo<int, string>.Create(-1, ex.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    return ResultInfo<int, string>.Create(-1, ex.ToString());
                }
            }
        }

        public ResultInfo<int, string> ExecuteTransaction(string[] CommandTexts,int timeout = 1800, Func<int, bool> action = null)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    using (DbTransaction tran = BeginTransaction(conn))
                    {
                        try
                        {
                            int rows = 0;
                            using (DbCommand cmd = CreateCommand(conn, null, tran))
                            {
                                cmd.CommandTimeout = timeout;
                                for (int i = 0; i < CommandTexts.Length; ++i)
                                {
                                    cmd.CommandText = CommandTexts[i];
                                    int erow = cmd.ExecuteNonQuery();

                                    if (action != null)
                                    {
                                        bool isContinue = action(erow);
                                        if (isContinue == false)
                                        {
                                            rows = 0;
                                            break;
                                        }
                                    }

                                    if (erow < 0)
                                    {
                                        rows = 0;
                                        break;
                                    }
                                    rows += erow;
                                }
                            }

                            if (rows > 0)
                            {
                                tran.Commit();
                            }
                            return new ResultInfo<int, string>(rows, string.Empty);
                        }
                        catch (Exception ex)
                        {
                            tran.Rollback();
                            return new ResultInfo<int, string>(-1, ex.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    return new ResultInfo<int, string>(-1, ex.ToString());
                }
            }
        }

        public ResultInfo<T, string> ExecuteScalar<T>(Parameter dbParameter)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    using (DbCommand cmd = CreateCommand(conn, dbParameter))
                    {
                        object obj = cmd.ExecuteScalar();

                        var rValue = GetReturnParameter(cmd);

                        return ResultInfo<T, string>.Create(obj == null ? default(T) : (T)Convert.ChangeType(obj, typeof(T)),
                          rValue == null ? string.Empty : rValue.ToString());
                    }
                }
                catch (Exception ex)
                {
                    return ResultInfo<T, string>.Create(default(T), ex.ToString());
                }
            }
        }

        public ResultInfo<int, string> BulkCopy(BulkParameter dbParameter)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    Exception temex = null;
                    int cnt = 0;
                    using (DbCommonBulkCopy bulkCopy = CreateBulkCopy(DbConnectionString, dbParameter.IsTransaction))
                    {
                        bulkCopy.Open();
                        bulkCopy.BulkCopiedHandler = dbParameter.BulkCopiedHandler;

                        bulkCopy.BatchSize = dbParameter.BatchSize;
                        bulkCopy.BulkCopyTimeout = dbParameter.ExecuteTimeout;

                        try
                        {
                            if ((dbParameter.DataSource == null
                                || dbParameter.DataSource.Rows.Count == 0)
                                && dbParameter.IDataReader != null)
                            {
                                bulkCopy.WriteToServer(dbParameter.IDataReader, dbParameter.TableName);
                                cnt = 1;
                            }
                            else
                            {
                                if (dbParameter.BatchSize > dbParameter.DataSource.Rows.Count)
                                    dbParameter.BatchSize = dbParameter.DataSource.Rows.Count;

                                bulkCopy.DestinationTableName = dbParameter.DataSource.TableName;
                                bulkCopy.WriteToServer(dbParameter.DataSource);
                                cnt = dbParameter.DataSource.Rows.Count;
                            }

                            //use transaction
                            if (dbParameter.IsTransaction)
                            {
                                //有事务回调则由外边控制事务提交,否则直接提交事务
                                if (dbParameter.TransactionCallback != null)
                                    dbParameter.TransactionCallback(bulkCopy.dbTrans, cnt);
                                else
                                    bulkCopy.dbTrans.Commit();
                            }
                        }
                        catch (Exception ex)
                        {
                            temex = ex;
                            if (dbParameter.IsTransaction)
                            {
                                if (bulkCopy.dbTrans != null) bulkCopy.dbTrans.Rollback();
                            }
                        }
                    }

                    if (temex != null) throw temex;
                    return ResultInfo<int, string>.Create(cnt, string.Empty);
                }
                catch (Exception ex)
                {
                    return ResultInfo<int, string>.Create(-1, ex.ToString());
                }
            }
        }

        public ResultInfo<List<T>, string> Query<T>(Parameter dbParameter) where T : new()
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    using (DbCommand cmd = CreateCommand(conn, dbParameter))
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows == false)
                            return ResultInfo<List<T>, string>.Create(new List<T>(1), string.Empty);

                        var items = reader.CreateObjects<T>(dbParameter.IgnoreCase);
                        return ResultInfo<List<T>, string>.Create(items, string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    return ResultInfo<List<T>, string>.Create(null, ex.ToString());
                }
            }
        }

        public ResultInfo<int, string> QueryChanged(Parameter dbParameter, Func<DataTable,bool> action)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                try
                {
                    using (DbConnection conn = CreateConnection(DbConnectionString))
                    using (DbCommand cmd = CreateCommand(conn, dbParameter))
                    using (DbDataAdapter adapter = CreateAdapter())
                    {
                        DataTable dt = new DataTable();
                        adapter.SelectCommand = cmd;
                        adapter.Fill(dt);

                        if (dt.Rows.Count == 0) return ResultInfo<int, string>.Create(0, "无可更新的数据行");

                        bool isContinue = action(dt);
                        if (isContinue == false) return ResultInfo<int, string>.Create(0, string.Empty);

                        DataTable changedt = dt.GetChanges(DataRowState.Added | DataRowState.Deleted | DataRowState.Modified);

                        if (changedt == null || changedt.Rows.Count == 0)
                            return ResultInfo<int, string>.Create(0, string.Empty);

                        using (DbCommandBuilder dbBuilder = this.CreateCommandBuilder())
                        {
                            dbBuilder.DataAdapter = adapter;
                            int rt = adapter.Update(changedt);
                            return ResultInfo<int, string>.Create(rt, string.Empty);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return ResultInfo<int, string>.Create(-1, ex.ToString());
                }
            }
        }
        #endregion

        #region private

        private object GetReturnParameter(DbCommand cmd)
        {
            if (cmd.Parameters != null && cmd.Parameters.Count > 0)
            {
                for(int i = 0; i < cmd.Parameters.Count; ++i)
                {
                    if (cmd.Parameters[i].Direction != ParameterDirection.Input)
                    {
                        return cmd.Parameters[i].Value;
                    }
                }
            }

            return null;
        }
        #endregion
    }
}
